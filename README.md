# Introdução

Sistema desenvolvido para mostrar habilidades técnicas utilizando o framework React.js

## Tecnologias utilizadas:

* `react-router` - Utilizado para rotear a aplicação por meio de pathname. Foi utilizado essa biblioteca pois senti uma necessidade de configurar manualmente as rotas que seriam publicas e privadas dentro da aplicação.
* `styled-components` - Estilos e CSS
* `redux-saga` - Utilizado pensando na escalabilidade da aplicação, que poderia sofrer com alguns problemas de side-effects dentro de um redux.
* `materialize-ui` - Utilizado pra agilizar na criação e manipulação de componentes cores, como por exemplo os inputs
* `flexbox` - Utilizado pensando na fácil criação de layouts responsivos
* `formik` & `yup` - Utilizados para validação de formulário de login da aplicação (Não senti necessidade de validar o input de search)
* `hooks` - Utilizando propriamente as melhores tecnologias do próprio react para manipulação de estados e ciclos de vida da propria aplicação


# Considerações finais:

Não foi utilizado nenhuma biblioteca e nenhum teste nessa aplicação pela minha grande falta de tempo nesses ultimos dias, mas se eu estivesse com mais tempo pra realiza-los, criaria alguns testes de regressão utilizando o `jest` e talvez um `enzyme` para validação dos proprios componentes.

Também queria utilizar `Typescript` mas a falta de tempo também não me proporcionou essa melhoria nos códigos, então eu optei por colocar alguns componentes com `Proptypes` para apenas uma segurança dentro dos componentes

Como sugestão de melhorias no código, certamente eu optaria por iniciar o código todo com Typescript e 'tipando' todos as funções e componentes para uma possivel facilidade de manutenção do código...
Também pensaria um pouco melhor em como dispor os componentes, tendo em vista que trabalho muito com a ideia de componentes `Statefull` e `Stateless`, mas atualmente também estou utilizando uma arquitetura voltada para Atomic Design, o que está me ajudando a enxergar muito melhor esses ciclos de componentes... Até consideraria utilizar sim.

Outra sugestão de melhoria pra essa aplicação seria utilizar um `redux-persist` para controlar melhor o localStorage e o próprio Redux em si, sem precisar de muita manutenção apenas em `localStorage.setItem()` e tudo mais...


Em relação a deploy dessa aplicação, utilizaria um framework que estou aprendendo atualmente que utiliza a própria Amazon pra subir os serviços, chamado `serverless`, configuraria o YML e apenas assistiria o deploy ser feito com apenas um comando, hahaha.

Acredito que seja isso, mais uma vez, obrigado pela oportunidade.
Até mais ;)




