import React from "react";
import { Route, Redirect } from "react-router-dom";

import * as Styled from "./styles";

function RouteWrapper({ component: Component, path = "", ...rest }) {
  return localStorage.getItem("access-token") === null ? (
    <Redirect from={path} to="/" />
  ) : (
    <Route
      {...rest}
      render={(props) => (
        <Styled.Main>
          <Styled.Body>
            <Component {...props} />
          </Styled.Body>
        </Styled.Main>
      )}
    />
  );
}

export default RouteWrapper;
