import styled from 'styled-components';

export const Main = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
`;

export const Sidebar = styled.div`
  width: 100%;
  height: 100%;
`;

export const Body = styled.div`
  /* width: calc(100% - 260px); */
  flex: 1;
  background: #eeeeee;
  min-height: 100vh;
`;

export const Loading = styled.div`
  ${({ show }) => (show ? 'display: flex;' : 'display: none;')}
  position: fixed;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: #00000088;
  justify-content: center;
  align-items: center;
`;
