import React, { useEffect } from "react";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import PrivateRoute from "./PrivateRoute.jsx";
import * as Styled from "./styles";

import { connect } from "react-redux";
import { ReactComponent as LoadingSVG } from "../assets/svg/loading.svg";

import Login from "../modules/Login";
import Search from "../modules/Search";

const Routes = ({ showLoading, history }) => {
  const token = localStorage.getItem("access-token");
  return (
    <>
      <Styled.Loading show={showLoading}>
        <LoadingSVG />
      </Styled.Loading>
      <BrowserRouter>
        <PrivateRoute
          exact
          path="/search"
          component={Search}
          history={history}
        />
        <Route
          path="/"
          exact
          component={() => {
            return token !== null ? (
              <Redirect from="/" to="/search" />
            ) : (
              <Login />
            );
          }}
        />
      </BrowserRouter>
    </>
  );
};

const mapStateToProps = (state) => ({
  showLoading: state.utilities.showLoading,
});

export default connect(mapStateToProps)(Routes);
