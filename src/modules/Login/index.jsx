import React from "react";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";

import { connect, useDispatch } from "react-redux";
import * as actionAuth from "../../store/modules/auth/actions";

import { useFormik } from "formik";
import * as Yup from "yup";

import * as Styled from "./styles";

import Logo from "../../assets/images/logo-home.png";

import { ReactComponent as IconCadeado } from "../../assets/svg/ic-cadeado.svg";
import { ReactComponent as IconEmail } from "../../assets/svg/ic-email.svg";

import PropTypes from "prop-types";

const Login = ({ errorMessage }) => {
  const dispatch = useDispatch();

  const { values, setFieldValue, handleSubmit, errors, touched } = useFormik({
    initialValues: { username: "", password: "" },
    validationSchema: Yup.object().shape({
      username: Yup.string().required("Email is required"),
      password: Yup.string().required("Password is required"),
    }),
    validateOnChange: true,
    validateOnBlur: false,
    onSubmit: async () => {
      dispatch(actionAuth.signInRequest(values.username, values.password));
    },
  });

  return (
    <Styled.MainLogin>
      <Styled.LogoContainer>
        <Styled.Logo src={Logo} />
      </Styled.LogoContainer>
      <Styled.Form onSubmit={handleSubmit}>
        <Styled.TitleBox>
          <Styled.Title>
            BEM-VINDO AO <br /> EMPRESAS
          </Styled.Title>
        </Styled.TitleBox>
        <Styled.DescriptionBox>
          <Styled.Description>
            Lorem ipsum dolor sit amet, contetur <br />
            adipiscing elit. Nunc accumsan.
          </Styled.Description>
        </Styled.DescriptionBox>
        <Styled.Box>
          <TextField
            onChange={(e) => setFieldValue("username", e.target.value)}
            label="E-mail"
            fullWidth
            error={touched.username && Boolean(errors.username)}
            helperText={touched.username ? errors.username : ""}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <IconEmail />
                </InputAdornment>
              ),
            }}
          />
        </Styled.Box>
        <Styled.Box>
          <TextField
            onChange={(e) => setFieldValue("password", e.target.value)}
            label="Senha"
            fullWidth
            type="password"
            error={touched.password && Boolean(errors.password)}
            helperText={touched.password ? errors.password : ""}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <IconCadeado />
                </InputAdornment>
              ),
            }}
          />
        </Styled.Box>

        {errorMessage !== "" && (
          <Styled.Box>
            <Styled.ErrorMessage>{errorMessage}</Styled.ErrorMessage>
          </Styled.Box>
        )}

        <Button
          type="submit"
          fullWidth
          color="primary"
          disabled={Object.keys(errors).length > 0}
        >
          ENTRAR
        </Button>
      </Styled.Form>
    </Styled.MainLogin>
  );
};

Login.propTypes = {
  errorMessage: PropTypes.string,
};

const mapStateToProps = (state) => ({
  errorMessage: state.auth.errorMessage,
});

export default connect(mapStateToProps)(Login);
