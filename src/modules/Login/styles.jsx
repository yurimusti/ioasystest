import styled from "styled-components";

export const MainLogin = styled.div`
  display: flex;
  background: #ebe9d7;
  width: 100%;
  height: 100vh;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const LogoContainer = styled.div`
  margin: 20px 0;
`;

export const Logo = styled.img``;

export const Form = styled.form`
  width: 30%;

  button {
    padding: 0.9rem 7.919rem 0.9rem 7.956rem;
    border-radius: 3.6px;
    background-color: #57bbbc;
    color: #fff;

    &:hover {
      background-color: #57bbbc;
    }

    &:disabled {
      opacity: 0.56;
      border-radius: 4px;
      background-color: #748383;
    }
  }
`;

export const TitleBox = styled.div`
  margin: 20px 0px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.span`
  width: 100%;
  font-family: Roboto;
  font-size: 1.5rem;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #383743;
`;
export const DescriptionBox = styled.div`
  margin: 20px 0px;
  display: flex;
  width: 100%;
`;

export const Description = styled.span`
  font-family: Roboto;
  font-size: 1.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.44;
  letter-spacing: -0.25px;
  color: #383743;
  width: 100%;
  text-align: center;
`;

export const Box = styled.div`
  margin: 20px 0px;
`;

export const ErrorMessage = styled.span`
  font-family: Roboto;
  font-size: 0.76rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.95;
  letter-spacing: -0.17px;
  text-align: center;
  color: #ff0f44;
`;
