import React, { useEffect, useState } from "react";
import { useDispatch, connect } from "react-redux";

import * as actionEnterprise from "../../store/modules/enterprise/actions";

import * as Styled from "./styles";

import PropTypes from "prop-types";
import Menu from "../../components/menu";
import Item from "../../components/item";
import Info from "../../components/info";

const Search = ({ listData }) => {
  const [id, setId] = useState("");
  const [title, setTitle] = useState("");
  const [searchValue, setSearchValue] = useState("");
  const [infoData, setInfoData] = useState({});
  const [list, setList] = useState([]);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actionEnterprise.getAllEnterprise());
  }, []);

  useEffect(() => {
    setList(listData.filter((e) => e.title.includes(searchValue)));
  }, [searchValue, listData]);

  useEffect(() => {
    setList(listData);
  }, [listData]);

  const _handleChangeValue = (ev) => {
    setSearchValue(ev.target.value);
  };

  const _handleSelectItem = (e) => {
    setInfoData(e);
    setId(e.id);
    setTitle(e.title);
  };

  const _handleBackPress = () => {
    setInfoData({});
    setId("");
    setTitle("");
  };

  const _renderList = () => {
    if (list.length !== 0) {
      return list.map((e, i) => (
        <Item
          title={e.title}
          country={e.country}
          description={e.description}
          photo={e.photo}
          key={i}
          onClick={() => _handleSelectItem(e)}
        />
      ));
    } else {
      return (
        <Styled.ContentTextInfo>
          <Styled.TextInfo>
            Nenhuma empresa foi encontrada para a busca realizada.
          </Styled.TextInfo>
        </Styled.ContentTextInfo>
      );
    }
  };

  const _renderInfo = () => {
    return <Info photo={infoData.photo} description={infoData.description} />;
  };

  return (
    <Styled.MainSearch listSize={list.length}>
      <Styled.MenuContent>
        <Menu
          id={id}
          title={title}
          onChange={_handleChangeValue}
          onBackPress={_handleBackPress}
        />
      </Styled.MenuContent>
      <Styled.ListContent>
        {id === "" ? _renderList() : _renderInfo()}
      </Styled.ListContent>
    </Styled.MainSearch>
  );
};

Search.propTypes = {
  listData: PropTypes.array,
};

const mapStateToProps = (state) => ({
  listData: state.enterprise.listEnterprise,
});

export default connect(mapStateToProps)(Search);
