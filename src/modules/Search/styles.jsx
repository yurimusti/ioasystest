import styled from "styled-components";

export const MainSearch = styled.div`
  display: flex;
  flex-direction: column;
  ${({ listSize }) => (listSize !== 0 ? `height: 100%;` : `height: 100vh;`)}
  background: #ebe9d7;
`;

export const MenuContent = styled.div`
  flex: 2;
`;

export const ListContent = styled.div`
  flex: 8;
`;

export const InfoContent = styled.div`
  flex: 8;
`;

export const ContentTextInfo = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const TextInfo = styled.div`
  font-family: Roboto;
  font-size: 2.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: center;
  color: #b5b4b4;
`;
