import styled from "styled-components";

export const ItemMain = styled.div`
  display: flex;
  flex-direction: row;
  background: #fff;
  margin: 3.18rem;
  padding: 1.68rem;
  height: 13.366rem;
  cursor: pointer;
`;

export const ContainerPhoto = styled.div`
  flex: 3;
  height: 100%;
`;

export const Photo = styled.img`
  height: 100%;
`;

export const ContainerInfo = styled.div`
  flex: 5;
  display: flex;
  flex-direction: column;
  margin: 1.43rem 0 0 2.4rem;
`;

export const Title = styled.span`
  font-family: Roboto;
  font-size: 1.875rem;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #1a0e49;
`;

export const Description = styled.span`
  font-family: Roboto;
  font-size: 1.5rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #8d8c8c;
`;

export const Country = styled.span`
  font-family: Roboto;
  font-size: 1.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #8d8c8c;
`;
