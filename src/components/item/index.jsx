import React from "react";

import * as Styled from "./styles";
import PropTypes from "prop-types";

const Menu = ({
  photo = "",
  title = "",
  description = "",
  country = "",
  onClick = () => {},
}) => {
  return (
    <Styled.ItemMain onClick={onClick}>
      <Styled.ContainerPhoto>
        <Styled.Photo src={photo} />
      </Styled.ContainerPhoto>
      <Styled.ContainerInfo>
        <Styled.Title>{title}</Styled.Title>
        <Styled.Description>{`${description.substring(
          0,
          250
        )}...`}</Styled.Description>
        <Styled.Country>{country}</Styled.Country>
      </Styled.ContainerInfo>
    </Styled.ItemMain>
  );
};

Menu.propTypes = {
  photo: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  country: PropTypes.string,
  onClick: PropTypes.func,
};

export default Menu;
