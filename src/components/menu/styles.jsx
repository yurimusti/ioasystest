import styled from "styled-components";

export const MenuMain = styled.div`
  position: relative;
  height: 9.438rem;
  background: rgb(238, 76, 119);
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1;
`;

export const MenuLinear = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: linear-gradient(
    180deg,
    rgba(238, 76, 119, 0.7749474789915967) 0%,
    rgba(13, 4, 48, 0.27354691876750703) 100%
  );
  z-index: 2;
`;

export const LogoContainer = styled.div`
  z-index: 3;
`;

export const Logo = styled.img``;

export const IconContainer = styled.div`
  z-index: 3;
  position: absolute;
  right: 32px;
  cursor: pointer;
`;

export const InputContent = styled.div`
  z-index: 3;
  width: 80%;
`;

export const InfoMenuMain = styled.div`
  display: flex;
  flex-direction: row;
  z-index: 3;
  width: 100%;
  padding-left: 48px;
`;

export const ContentIcon = styled.div`
  cursor: pointer;
  svg {
    width: 48px;
    height: 48px;
    fill: #fff;
  }
`;

export const ContentTitle = styled.div`
  margin-left: 50px;
`;

export const Title = styled.span`
  font-family: Roboto;
  font-size: 2.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #fff;
`;
