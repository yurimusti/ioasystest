import React, { useState, useEffect } from "react";

import Logo from "../../assets/images/logo-nav.png";
import { ReactComponent as IconSearch } from "../../assets/svg/ic-search.svg";
import { ReactComponent as IconArrow } from "../../assets/svg/left-arrow.svg";

import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";

import * as Styled from "./styles";
import PropTypes from "prop-types";

const Menu = ({
  title = "",
  onChange = () => {},
  onOpenSearch = () => {},
  id = "",
  onBackPress = () => {},
}) => {
  const [openSearch, setOpenSearch] = useState(false);
  const [_id, setId] = useState("");

  useEffect(() => {
    setId(id);
  }, [id]);

  const _handleOpenSearchInput = () => {
    setOpenSearch(true);
    onOpenSearch(openSearch);
  };

  const _handleChangeInput = (ev) => {
    onChange(ev);
  };

  const _handleBack = () => {
    onBackPress();
    setOpenSearch(false);
  };

  const _renderInfo = () => {
    return (
      <Styled.InfoMenuMain>
        <Styled.ContentIcon onClick={_handleBack}>
          <IconArrow />
        </Styled.ContentIcon>
        <Styled.ContentTitle>
          <Styled.Title>{title}</Styled.Title>
        </Styled.ContentTitle>
      </Styled.InfoMenuMain>
    );
  };

  const _renderSearch = () => {
    return openSearch ? (
      <Styled.InputContent>
        <TextField
          onChange={_handleChangeInput}
          label="Pesquisa"
          fullWidth
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <IconSearch style={{ width: 32 }} />
              </InputAdornment>
            ),
          }}
        />
      </Styled.InputContent>
    ) : (
      <>
        <Styled.LogoContainer>
          <Styled.Logo src={Logo} />
        </Styled.LogoContainer>

        <Styled.IconContainer onClick={_handleOpenSearchInput}>
          <IconSearch />
        </Styled.IconContainer>
      </>
    );
  };

  return (
    <Styled.MenuMain>
      <Styled.MenuLinear />
      {_id !== "" ? _renderInfo() : _renderSearch()}
    </Styled.MenuMain>
  );
};

Menu.propTypes = {
  title: PropTypes.string,
  onChange: PropTypes.func,
  openSearch: PropTypes.func,
  id: PropTypes.string,
  onBackPress: PropTypes.func,
};

export default Menu;
