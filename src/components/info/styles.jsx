import styled from "styled-components";

export const InfoMain = styled.div`
  height: 60.338rem;
  border-radius: 4.8px;
  background-color: #fff;
  display: flex;
  flex-direction: column;
  margin: 3.125rem;
`;

export const ImageContent = styled.div`
  margin: 3rem 4.68rem;
`;

export const Image = styled.img`
  width: 100%;
`;

export const DescriptionContent = styled.div`
  margin: 3rem 4.68rem;
`;

export const Description = styled.span`
  font-family: "Source Sans Pro", sans-serif;
  font-size: 2.125rem;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
`;
