import React from "react";
import * as Styled from "./styles";
import PropTypes from "prop-types";

const Info = ({ photo, description }) => {
  return (
    <Styled.InfoMain>
      <Styled.ImageContent>
        <Styled.Image src={photo} />
      </Styled.ImageContent>
      <Styled.DescriptionContent>
        <Styled.Description>{description}</Styled.Description>
      </Styled.DescriptionContent>
    </Styled.InfoMain>
  );
};

Info.propTypes = {};

export default Info;
