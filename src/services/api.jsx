import axios from "axios";
import history from "./history";
import { store } from "../store";
import * as actionAuth from "../store/modules/auth/actions";
import { API_URL } from "../constants";

const api = axios.create({
  baseURL: `${API_URL}`,
});

api.interceptors.response.use(
  function (response) {
    return Promise.resolve(response);
  },
  function (error) {
    if (error.response.status === 401) {
      store.dispatch(actionAuth.logout());
    }
    return Promise.reject(error);
  }
);

api.interceptors.request.use(async (config) => {
  const accessToken = localStorage.getItem("access-token");
  const client = localStorage.getItem("client");
  const uid = localStorage.getItem("uid");

  if (accessToken !== null && client !== null && uid !== null) {
    config.headers["access-token"] = `${accessToken}`;
    config.headers["client"] = `${client}`;
    config.headers["uid"] = `${uid}`;
  }
  return config;
});

export default api;
