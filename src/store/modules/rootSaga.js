import { all } from "redux-saga/effects";

import auth from "./auth/sagas";
import utilities from "./utilities/sagas";
import enterprise from "./enterprise/sagas";

export default function* rootSaga() {
  return yield all([auth, utilities, enterprise]);
}
