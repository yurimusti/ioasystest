export function showLoading(show) {
  return {
    type: '@utilities/SHOW_LOADING',
    payload: { show },
  };
}

export function deslogar() {
  return {
    type: '@auth/LOGOUT',
  };
}
