const INITIAL_STATE = {
  showLoading: false,
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case '@utilities/SHOW_LOADING':
      return {
        ...state,
        showLoading: action.payload.show,
      };

    case '@auth/LOGOUT':
      return INITIAL_STATE;
    default:
      return state;
  }
}
