import { combineReducers } from "redux";

import auth from "./auth/reducer";
import utilities from "./utilities/reducer";
import enterprise from "./enterprise/reducer";

export default combineReducers({
  auth,
  utilities,
  enterprise,
});
