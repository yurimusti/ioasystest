import { takeLatest, call, put, all } from "redux-saga/effects";

import { push } from "react-router-redux";
import { authUser } from "./call";

export function* signIn({ payload }) {
  yield put({
    type: "@utilities/SHOW_LOADING",
    payload: { show: true },
  });

  try {
    const { data, headers } = yield call(authUser, payload);
    localStorage.setItem("access-token", headers["access-token"]);
    localStorage.setItem("client", headers["client"]);
    localStorage.setItem("uid", headers["uid"]);

    yield put(push("/search"));

    yield put({
      type: "@auth/SIGN_IN_SUCCESS",
      payload: { user: data },
    });

    yield put({
      type: "@auth/LOGIN_ERROR",
      payload: { message: "" },
    });

    yield put({
      type: "@utilities/SHOW_LOADING",
      payload: { show: false },
    });
  } catch (e) {
    if (e.response.status === 401) {
      yield put({
        type: "@auth/LOGIN_ERROR",
        payload: { message: e.response.data.errors[0] },
      });
    }
  } finally {
    yield put({
      type: "@utilities/SHOW_LOADING",
      payload: { show: false },
    });
  }
}

export function* logoutUserSaga() {
  localStorage.clear();
  yield put({
    type: "@auth/LOGOUT",
  });
  window.location.reload();
}

export default all([
  takeLatest("@auth/SIGN_IN_REQUEST", signIn),
  takeLatest("@auth/LOGOUT_USER", logoutUserSaga),
]);
