export function signInRequest(email, password) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: { email, password },
  };
}

export function logout(payload) {
  return {
    type: '@auth/LOGOUT_USER',
    payload,
  };
}

