import axios from "axios";
import { API_URL } from "../../../constants";

export const authUser = (params) => {
  return new Promise((resolve, reject) => {
    axios
      .post(`${API_URL}/users/auth/sign_in`, params)
      .then((e) => resolve(e))
      .catch((err) => reject(err));
  });
};
