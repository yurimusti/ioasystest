const INITIAL_STATE = {
  listEnterprise: [],
};

export default function auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "@enterprise/LIST_SUCCESS":
      return {
        ...state,
        listEnterprise: action.payload.listEnterprise,
      };
    case "@auth/LOGOUT":
      return INITIAL_STATE;
    default:
      return state;
  }
}
