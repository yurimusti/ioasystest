import { takeLatest, call, put, all } from "redux-saga/effects";

import { getAllEnterprisesCall, getSearchEnterprisesCall } from "./call";

const BASE_URL_IMAGE = "http://empresas.ioasys.com.br";

const formatData = (arr = []) => {
  return arr.map((e) => ({
    id: e.id,
    title: e.enterprise_name,
    description: e.description,
    photo: `${BASE_URL_IMAGE}${e.photo}`,
    country: e.city,
  }));
};

export function* getAllEnterprisesSaga({ payload }) {
  yield put({
    type: "@utilities/SHOW_LOADING",
    payload: { show: true },
  });
  try {
    const { data } = yield call(getAllEnterprisesCall);
    const listEnterprise = formatData(data.enterprises);

    yield put({
      type: "@enterprise/LIST_SUCCESS",
      payload: { listEnterprise },
    });
  } catch (e) {
    console.log(e);
  } finally {
    yield put({
      type: "@utilities/SHOW_LOADING",
      payload: { show: false },
    });
  }
}

export default all([
  takeLatest("@enterprise/GET_ALL_ENTERPRISE", getAllEnterprisesSaga),
  // takeLatest("@auth/LOGOUT_USER", logoutUserSaga),
]);
