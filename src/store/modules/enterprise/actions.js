export function getAllEnterprise() {
  return {
    type: "@enterprise/GET_ALL_ENTERPRISE",
  };
}

export function getSearchEnterprise(payload) {
  return {
    type: "@enterprise/GET_SEARCH_ENTERPRISE",
    payload,
  };
}
