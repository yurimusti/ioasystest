import API from "../../../services/api";
import { API_URL } from "../../../constants";

export const getAllEnterprisesCall = (params) => {
  return new Promise((resolve, reject) => {
    API.get(`${API_URL}/enterprises`, params)
      .then((e) => resolve(e))
      .catch((err) => reject(err));
  });
};

export const getSearchEnterprisesCall = (params) => {
  return new Promise((resolve, reject) => {
    API.get(`${API_URL}/enterprises?name=${params.name}`)
      .then((e) => resolve(e))
      .catch((err) => reject(err));
  });
};
