import { createStore, compose, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";

export default (reducers, middlewares) => {
  const enhancer = compose(
    composeWithDevTools(applyMiddleware(...middlewares))
  );

  return createStore(reducers, enhancer);
};
